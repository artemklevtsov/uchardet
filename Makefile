PKG_NAME = $(shell grep -i '^package' DESCRIPTION | cut -d ':' -d ' ' -f 2)
PKG_VERSION = $(shell grep -i '^version' DESCRIPTION | cut -d ':' -d ' ' -f 2)

TARBALL = $(PKG_NAME)_$(PKG_VERSION).tar.gz
CHECK_DIR = $(PKG_NAME).Rcheck

R_FILES := $(wildcard R/*.R)
SRC_FILES := $(wildcard src/*) $(addprefix src/, $(COPY_SRC))
PKG_FILES := DESCRIPTION NAMESPACE README.md $(R_FILES) $(SRC_FILES)

RCMD := $(shell which R) --vanilla --quiet CMD
RSCRIPT := $(shell which Rscript)  --vanilla

BUILD_ARGS := --no-manual
CHECK_ARGS := --no-manual --as-cran --run-donttest
INSTALL_ARGS := --preclean --clean --strip

export _R_CHECK_CRAN_INCOMING_ := false
export _R_CHECK_FORCE_SUGGESTS_ := true
export _R_CHECK_LENGTH_1_CONDITION_ := "true"
export _R_CHECK_LENGTH_1_LOGIC2_ := "true"

all: doc check

$(TARBALL): $(PKG_FILES)
	$(RCMD) build $(BUILD_ARGS) .

.PHONY: doc
doc: $(R_FILES)
	$(RSCRIPT) -e 'roxygen2::roxygenize()'

.PHONY: check
check: $(TARBALL)
	$(RCMD) check $(CHECK_ARGS) $(TARBALL)

.PHONY: build
build: $(TARBALL)

.PHONY: pacakge
pacakge: $(TARBALL)
	$(RCMD) INSTALL --build $(TARBALL)

.PHONY: install
install: $(TARBALL)
	$(RCMD) INSTALL $(INSTALL_ARGS) $(TARBALL)

.PHONY: uninstall
uninstall:
	(RCMD) REMOVE $(PKG_NAME)

.PHONY: manual
manual: doc
	$(RCMD) Rd2pdf --no-preview --force -o $(PKG_NAME)_$(PKG_VERSION)-manual.pdf .

.PHONY: test
test: inst/tinytest
	$(RSCRIPT) -e 'tinytest::build_install_test()'

README.md: README.Rmd
	Rscript -e "rmarkdown::render('$<', output_file = '$@', quiet = TRUE)" || rm "$@"

.PHONY: clean
clean:
	$(RM) $(TARBALL)
	$(RM) -r $(CHECK_DIR)
	$(RM) -r autom4te.cache
	$(RM) config.*
	$(RM) src/*.o
	$(RM) src/*.so
	$(RM) src/*.dll
	$(RM) src/uchardet/src/*.o
	$(RM) src/uchardet/src/*.so
	$(RM) src/uchardet/src/*.a
	$(RM) src/uchardet/src/LangModels/*.o
	$(RM) vignettes/*.md
	$(RM) vignettes/*.html
	$(RM) vignettes/*.pdf
	$(RM) README.html
	$(RM) *.tar.gz
	$(RM) $(PKG_NAME)_$(PKG_VERSION)-manual.pdf
	find . -name '.Rhistory' -delete
