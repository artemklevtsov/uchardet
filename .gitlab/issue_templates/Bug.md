### Summary

(Summarize the bug encountered concisely)

### Steps to reproduce

(How one can reproduce the issue - this is very important)

(Please exclude the prompt `>` from the code. Including it means it won't work when we copy and paste it to our prompt. Use `#` to comment the output parts.)

### Expected behavior

(What you should see instead)

### Actual behavior

(What actually happens)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

#### Traceback

(Paste `traceback()` command output - please use code blocks (```) to format console output.)

#### R and packages versions

(Paste `sessionInfo()`or `devtools::session_info()` command output  - please use code blocks (```) to format console output.)
